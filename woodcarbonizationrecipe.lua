--woodcarbonizationrecipe.lua , this is the DATA.LUA stage
-- by Dr. Beco
-- 2023-01-22

local woodcarbonization = {
    type = "recipe",
    name = "woodcarbonization",
    localised_name = { "", {"woodcarbonization.recipename"}},
    localised_description = { "", {"woodcarbonization.recipedescription"}},
    category = "smelting",
    hide_from_player_crafting = true,
    allow_as_intermediate = false,

    normal =
    {
        enabled = not settings.startup["woodcarbonization-research"].value,
        energy_required = 10,
        ingredients = {{"wood", settings.startup["woodcarbonization-price"].value}},
        result = "coal",
        result_count = settings.startup["woodcarbonization-issue"].value,
    },
    expensive =
    {
        -- on expensive mode it always need research to unlock
        enabled = false,
        energy_required = 20,
        ingredients = {{"wood", 2*settings.startup["woodcarbonization-price"].value}},
        result = "coal",
        result_count = settings.startup["woodcarbonization-issue"].value,
    }
}

data:extend({woodcarbonization})

table.insert(data.raw.technology["advanced-material-processing"].effects, { type = "unlock-recipe", recipe = "woodcarbonization" })

if(settings.startup["woodcarbonization-woodasfuel"].value == false ) then
    --   Wood item default values:
    --   fuel_value = "2MJ",
    --   fuel_category = "chemical",
    data.raw.item["wood"].fuel_value = nil
    data.raw.item["wood"].fuel_category = nil
end

