# Make for Woodcarbonization Factorio Module
# Copyright (C) 2023 by Ruben Carlo Benante
# Contact: rcb@beco.cc
# Date: 2023-01-22

VERSION:=$(shell cat info.json | grep \"version\": | cut -d'"' -f4)

zip:
	echo "Creating WoodCarbonization mod Version $(VERSION)"
	if [ -f woodcarbonization_$(VERSION).zip ] ; then echo removing old zip woodcarbonization_$(VERSION).zip ; rm -f woodcarbonization_$(VERSION).zip ; fi
	cd .. && zip -r woodcarbonization_$(VERSION).zip woodcarbonization -x@woodcarbonization/excludefromzip.lst && mv woodcarbonization_$(VERSION).zip woodcarbonization/

install:
	echo Removing old version from ~/.config/factorio/mods
	rm -f ~/.config/factorio/mods/woodcarbonization_*.zip
	echo Copying woodcarbonization_$(VERSION).zip to ~/.config/factorio/mods
	cp woodcarbonization_$(VERSION).zip ~/.config/factorio/mods

