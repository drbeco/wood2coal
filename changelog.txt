---------------------------------------------------------------------------------------------------
Version: 1.3.2
Date: 2023-12-17
  Bugfixes:
    - Exclude from zip (excludefromzip.lst) updated
---------------------------------------------------------------------------------------------------
Version: 1.3.1
Date: 2023-12-17
  Minor Features:
    - Updated thumbnail
---------------------------------------------------------------------------------------------------
Version: 1.3.0
Date: 2023-12-17
  Major Features:
    - Settings for proportion of wood / coal
---------------------------------------------------------------------------------------------------
Version: 1.2.0
Date: 2023-01-24
  Bugfixes:
    - Fix adding all old zipfiles versions into current version zipfile
---------------------------------------------------------------------------------------------------
Version: 1.1.0
Date: 2023-01-24
  Bugfixes:
    - Unlock tech appended to the table instead of a simple attribution
    - changelog format fix for appearing in-game
---------------------------------------------------------------------------------------------------
Version: 1.0.1
Date: 2023-01-23
  Minor Features:
    - Updated changelog
  TODO:
    - homepage
---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 2023-01-23
  Major Features:
    - Settings button woodcarbonization-research now working
    - Settings button woodcarbonization-woodasfuel now working
  Features:
    - Improved locale for buttons
  Bugfixes:
    - Inverted true/false for woodcarbonization-research to improve redability
    - Cleaned unnecessary code comments
---------------------------------------------------------------------------------------------------
Version: 0.2.1
Date: 2023-01-23
  Bugfixes:
    - Version 0.2.0 is actually from 2023-01-23
    - Added description note on enabled button to explain it is inactive on expensive mode
  Code Organization:
    - Cleaned debug comments
---------------------------------------------------------------------------------------------------
Version: 0.2.0
Date: 2023-01-22
  Major Features:
    - The settings enable button is implemented. If enabled, no need to research advanced material processing beforehand
  Bugfixes:
    - Added migration 20230122-migration.json for recipe name changed
    - Settings are now from the startup category
  Code Organization:
    - Added git tag to all versions
    - Updaded changelog.txt to reflect all versions correctly
    - Removed image file, left only thumbnail as expected by the game
  TODO:
    - Implement the wood as fuel settings
---------------------------------------------------------------------------------------------------
Version: 0.1.6
Date: 2023-01-22
  Code Organization:
    - Added this file (changelog.txt)
    - Added thumbnail
  TODO:
    - Add more details to changelog.txt
    - Add AUTHORS file
---------------------------------------------------------------------------------------------------
Version: 0.1.5
Date: 2023-01-22
  Major Features:
    - woodcarbonizationrecipe.lua new recipe result_count = 6 to balance the gameplay
    - Recipe ingredients 2 woods, 10 seconds
    - Recipe in expensive mode 2 woods, 20 seconds, result_count = 2
  Code Organization:
    - beta version 0.1.5
  Bugfixes:
    - renamed recipe file woodcarbonizationrecipe.lua
    - changed data.lua requires woodcarbonizationrecipe
    - renamed locale variables to reflect woodcarbonization prefix
  TODO:
    - Implement the settings variables
    - Add thumbnail
    - Organize folders
    - Add changelog.txt
---------------------------------------------------------------------------------------------------
Version: 0.1.4
Date: 2023-01-22
  Bugfixes:
    - locale variables renamed
    - Changed category name in woodcarbonization-locale.cfg
    - Adding subdirectory to patterns in excludefromzip.lst
  Code Organization:
    - data.lua to call new recipe file name, renamed recipe file
    - beta version 0.1.4
---------------------------------------------------------------------------------------------------
Version: 0.1.3
Date: 2023-01-22
  Features:
    - Changed name of module from wood2coal to woodcarbonization due to conflict with existing mods
    - Added conditional remove of old zips in makefile
    - Added makefile target install to copy zip to mods game folder
  Bugfixes:
    - files to exclude from zip
    - changed repository URL
    - changed wood2coal-locale.cfg to woodcarbonization-locale.cfg
    - changed name in LICENSE
    - changed name in coalrecipe.lua
    - Added allow_as_intermediate = false, to prevent recipe in handcrafting other things
    - Changed title in info.json "Wood Carbonization"
    - Changed name in makefile
    - Changed name in settings.lua
    - Makefile bugfix zip with list of ignored files
    - Makefile bugfix target install with rm -f to prevend failing when not removing old files
  Code Organization:
    - Makefile zip exclude from file
    - Added gitignore
    - Added excludefromzip.lst
    - Removed order in settings.lua
---------------------------------------------------------------------------------------------------
Version: 0.1.2
Date: 2023-01-22
  Features:
    - locale for recipe in coalrecipe.lua
    - Autoversion in makefile from info.json
    - Added a second checkbox on settings.lua for enabling mod in god-mode
  Code Organization:
    - Tag release on gitlab as v0.1.2
---------------------------------------------------------------------------------------------------
Version: 0.1.1
Date: 2023-01-22
  Features:
    - make target zip
    - zip mod_version.zip
  Bugfixes:
    - data.lua requires coalrecipe
    - info.json description improved
    - makefile zip bugfix mod_version (not mod-version)
    - locale bugfix category for recipename and recipedescription
    - locale added settings description
---------------------------------------------------------------------------------------------------
Version: 0.1.0
Date: 2023-01-22
  Major Features:
    - Wood2Coal commit zero
    - License MIT
    - Recipe with result_count = 12, 1 wood, 16 seconds
    - Added expensive recipe result_count = 12, 2 woods, 32 seconds
    - data.lua requires prototypes.coalrecipe
    - info.json recipe name wood2coal (a conflict fixed later in v0.1.3)
    - locale for recipe with variable names and recipe-name (an error fixed later)
    - Settings with order value
  Code Organization:
    - Added LICENSE
    - Added recipe coalrecipe.lua
    - Added data.lua
    - Added info.json
    - Added wood2coal-locale.cfg
    - Added settings.lua
    - Added wood2coal-icon.png

