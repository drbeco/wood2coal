-- settings.lua
-- by Dr. Beco
-- 2023-01-22

data:extend({
  {
    type = "bool-setting",
    name = "woodcarbonization-woodasfuel",
    setting_type = "startup",
    default_value = true,
  },
  {
    type = "bool-setting",
    name = "woodcarbonization-research",
    setting_type = "startup",
    default_value = true,
  },
  {
    type = "int-setting",
    name = "woodcarbonization-price",
    setting_type = "startup",
    default_value = 4,
    minimum_value = 1,
    maximum_value = 16,
    allowed_values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
  },
  {
    type = "int-setting",
    name = "woodcarbonization-issue",
    setting_type = "startup",
    default_value = 2,
    minimum_value = 1,
    maximum_value = 8,
    allowed_values = {1, 2, 3, 4, 5, 6, 7, 8},
  },
})

